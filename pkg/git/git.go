package git

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/ashishkujoy/housekeeping/pkg/utils"
	"os/exec"
)

const gitLocalReposPath = "/Users/ashishkumar/.myAppConfig/gitLocalRepos"

type LocalGitRepo struct {
	Name         string `json:"name"`
	AbsolutePath string `json:"absolutePath"`
}

type LocalGitRepos = []LocalGitRepo

func (localGitRepo LocalGitRepo) TakeFreshPull() {
	fmt.Printf("\nRefreshing %s", localGitRepo.Name)
	stdErr := bytes.Buffer{}
	gitPullCommand := exec.Command("git", "pull", "-r", "--autostash")
	gitPullCommand.Dir = localGitRepo.AbsolutePath
	gitPullCommand.Stderr = &stdErr

	gitPullCommandError := gitPullCommand.Run()

	if gitPullCommandError != nil {
		fmt.Printf("Unable to take pull due to %s", stdErr.String())
	}
	println(":       Done")
}

func TakeFreshPullIn(localGitRepos LocalGitRepos) {
	for _, localGitRepo := range localGitRepos {
		localGitRepo.TakeFreshPull()
	}
}

func ReadLocalGitRepoFromConfig() []LocalGitRepo {
	var localGitRepo []LocalGitRepo
	_ = utils.CreateDirectoryIfNotExists("/Users/ashishkumar/.myAppConfig/")
	contents := utils.ReadFileWithDefaultContents(gitLocalReposPath, "[]")
	_ = json.Unmarshal(contents, &localGitRepo)
	return localGitRepo
}

func RefreshRepos() {
	config := ReadLocalGitRepoFromConfig()
	fmt.Printf("\nRefreshing %d repository\n", len(config))
	TakeFreshPullIn(config)
}

func RegisterCurrentRepo() {
	repo := LocalGitRepo{
		Name:         utils.CurrentDirectoryName(),
		AbsolutePath: utils.Pwd(),
	}

	localGitRepos := append(ReadLocalGitRepoFromConfig(), repo)
	marshal, err := json.Marshal(localGitRepos)
	utils.PanicOnError(err)
	utils.WriteFile(gitLocalReposPath, marshal)
}
