package main

import (
	"github.com/ashishkujoy/housekeeping/pkg/cli"
	"os"
)

func main() {
	cli.Execute(os.Args[1])
}
