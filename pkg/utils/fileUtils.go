package utils

import (
	"io/ioutil"
	"os"
	"strings"
)

func CreateDirectoryIfNotExists(dirname string) error {
	if _, err := os.Stat(dirname); err == nil {
		return nil
	}

	if err := os.Mkdir(dirname, 0777); err != nil {
		return err
	}

	return nil
}

func ReadFileWithDefaultContents(filePath, defaultContent string) []byte {
	if _, err := os.Stat(filePath); err != nil {
		println("Writing to the file")
		_, _ = os.Create(filePath)
		writeErr := ioutil.WriteFile(filePath, []byte(defaultContent), 0644)
		if writeErr != nil {
			println(writeErr.Error())
		}
		return []byte(defaultContent)
	}
	fileContent, _ := ioutil.ReadFile(filePath)
	return fileContent
}

func CreateFileIfNotExists(filePath string) {
	if _, err := os.Stat(filePath); err != nil {
		_, errOnCreateFile := os.Create(filePath)
		PanicOnError(errOnCreateFile)
	}
}

func WriteFile(filePath string, content []byte) {
	CreateFileIfNotExists(filePath)
	writeErr := ioutil.WriteFile(filePath, content, 0644)
	PanicOnError(writeErr)
}

type MarshallAble interface {
	Marshall() []byte
}

func Pwd() string {
	pwd, err := os.Getwd()
	PanicOnError(err)
	return pwd
}

func CurrentDirectoryName() string {
	split := strings.Split(Pwd(), "/")
	return split[len(split)-1]
}

func PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
