package cli

import (
	"fmt"
	"os"
)

func isNoOptionProvided() bool {
	return len(os.Args) < 2
}

func showHelp() {
	refreshAllRepos := "Refresh all register git repos."
	registerRepo := "Registers a git repo."
	fmt.Printf("%s\n\n%s\n", refreshAllRepos, registerRepo)
}
