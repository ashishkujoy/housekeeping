package cli

import "github.com/ashishkujoy/housekeeping/pkg/git"

func Execute(arg string) {
	switch arg {
	case RefreshAllGitRepos:
		git.RefreshRepos()
	case RegisterRepo:
		git.RegisterCurrentRepo()
	}
}

const (
	RefreshAllGitRepos = "refresh"
	RegisterRepo       = "register"
)
