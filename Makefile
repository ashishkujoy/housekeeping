PKGS := $(shell go  list ./... | grep -v /vendor)

.PHONY: test
test:
	go test -cover $(PKGS)

housekeeping:
	CGO_ENABLED=0 go build -o bin/housekeeping ./pkg/main/

.PHONY: housekeeping_stripped
housekeeping_stripped:
	go build -o bin/housekeeping -ldflags="-s -w" ./pkg/main/

.PHONY: housekeeping_compressed
housekeeping_compressed: housekeeping_stripped
	upx bin/housekeeping